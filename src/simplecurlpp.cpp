/**
 * @file simplecurlpp.cpp
 * @author joonatoona
 * @date 4 Mar 2018
 * @brief Main source for SimpleCurlPP
 */

#include "simplecurlpp.h"

#include <curl/curl.h>
#include <curl/easy.h>
#include <sstream>

using namespace SimpleCurlPP;

NetworkRequest::NetworkRequest(const std::string &url, const RequestType &type) {
    m_url = url;
    m_type = type;
    m_curl = curl_easy_init();
    curl_easy_setopt((CURL *)m_curl, CURLOPT_URL, url.c_str());
}

NetworkRequest::~NetworkRequest() { curl_easy_cleanup((CURL *)m_curl); }

void NetworkRequest::setBody(const std::string &body, const BodyType &bodyType) {
    setHeader("Content-Type", BODY_TYPE_HEADER.at(bodyType));
    m_body = body;
}

void NetworkRequest::setHeader(const std::string &header, const std::string &value) {
    m_rmHeaders.erase(header);
    m_headers[header] = value;
}

void NetworkRequest::unsetHeader(const std::string &header) {
    m_headers.erase(header);
    m_rmHeaders.insert(header);
}

static size_t writeData(void *ptr, size_t size, size_t nmemb, void *stream) {
    ((std::stringstream *)stream)->write((const char *)ptr, size * nmemb);
    return size * nmemb;
}

NetworkResponse NetworkRequest::send() {
    NetworkResponse netRes;

    curl_easy_setopt((CURL *)m_curl, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt((CURL *)m_curl, CURLOPT_NOSIGNAL, 1);
    curl_easy_setopt((CURL *)m_curl, CURLOPT_ACCEPT_ENCODING, "");

    struct curl_slist *chunk = nullptr;

    if (m_headers.find("User-Agent") == m_headers.end() && m_rmHeaders.find("User-Agent") == m_rmHeaders.end()) {
        char userAgent[1024];
        snprintf(userAgent, sizeof(userAgent), "User-Agent: libcurl/%s", curl_version_info(CURLVERSION_NOW)->version);
        userAgent[sizeof(userAgent) - 1] = 0;
        chunk = curl_slist_append(chunk, userAgent);
    }

    for (const auto &header : m_headers) {
        std::string headerString = header.first + (header.second.empty() ? ";" : ": " + header.second);
        chunk = curl_slist_append(chunk, headerString.c_str());
    }

    for (const auto &header : m_rmHeaders)
        chunk = curl_slist_append(chunk, (header + ":").c_str());

    curl_easy_setopt((CURL *)m_curl, CURLOPT_HTTPHEADER, chunk);

    std::stringstream out;
    curl_easy_setopt((CURL *)m_curl, CURLOPT_WRITEFUNCTION, writeData);
    curl_easy_setopt((CURL *)m_curl, CURLOPT_WRITEDATA, &out);

    std::stringstream hout;
    curl_easy_setopt((CURL *)m_curl, CURLOPT_HEADERFUNCTION, writeData);
    curl_easy_setopt((CURL *)m_curl, CURLOPT_WRITEHEADER, &hout);

    // TODO: Refactor
    if (m_type == RequestType::POST) {
        curl_easy_setopt((CURL *)m_curl, CURLOPT_POST, 1);
        curl_easy_setopt((CURL *)m_curl, CURLOPT_POSTFIELDS, m_body.c_str());
        curl_easy_setopt((CURL *)m_curl, CURLOPT_POSTFIELDSIZE, m_body.size());
    }

    CURLcode res = curl_easy_perform((CURL *)m_curl);
    if (res != CURLE_OK)
        netRes.error = curl_easy_strerror(res);

    curl_slist_free_all(chunk);

    netRes.body = out.str();
    curl_easy_getinfo((CURL *)m_curl, CURLINFO_RESPONSE_CODE, &netRes.status);

    char *url;
    curl_easy_getinfo((CURL *)m_curl, CURLINFO_EFFECTIVE_URL, &url);
    if (!url)
        netRes.url = m_url;
    else
        netRes.url = url;

    // TODO: Move to seperate function
    std::string header;
    while (std::getline(hout, header) && header != "\r") {
        size_t index = header.find(":", 0);
        if (index == std::string::npos)
            continue;
        netRes.headers.emplace(header.substr(0, index), header.substr(index + 2));
    }

    return netRes;
}

std::ostream &SimpleCurlPP::operator<<(std::ostream &os, const NetworkResponse &rs) {
    os << rs.url << " (" << rs.status << ")";
    return os;
}

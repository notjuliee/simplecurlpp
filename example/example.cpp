/**
 * @file example.cpp
 * @brief Example usage of simplecurlpp
 */

#include <simplecurlpp.h>

#include <iostream>

using namespace SimpleCurlPP;

int main() {
    std::cout << "Testing GET\n" << std::endl;

    //! [Executing a GET request]
    NetworkRequest req("https://httpbin.org/get");
    req.setHeader("X-Foo", "Bar");

    NetworkResponse res = req.send();
    //! [Executing a GET request]

    std::cout << res << std::endl;
    std::cout << res.body << std::endl;

    for (const auto &h : res.headers)
        std::cout << h.first << ": " << h.second << std::endl;

    std::cout << "\nTesting POST\n" << std::endl;

    //! [Executing a POST request]
    NetworkRequest postReq("https://httpbin.org/post", RequestType::POST);
    postReq.setBody("{\"a\": 1}", BodyType::JSON);

    NetworkResponse postRes = postReq.send();
    //! [Executing a POST request]

    std::cout << res << std::endl;
    std::cout << res.body << std::endl;

    return 0;
}

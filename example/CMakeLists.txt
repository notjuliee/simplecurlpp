cmake_minimum_required(VERSION 3.5.2)

project(SCPPExample)

set(CMAKE_CXX_STANDARD 11)

add_subdirectory("../" "${CMAKE_CURRENT_BINARY_DIR}/scpp")

add_executable(SCPPExample "example.cpp")
target_include_directories(SCPPExample PRIVATE ${SIMPLECURLPP_INCLUDE_DIRS})

target_link_libraries(SCPPExample
    simplecurlpp
)

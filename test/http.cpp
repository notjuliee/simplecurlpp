#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "simplecurlpp.h"

using namespace SimpleCurlPP;

TEST_CASE("GET works", "[networkrequest]") {
    NetworkRequest req("https://httpbin.org/get");
    NetworkResponse res = req.send();

    REQUIRE(res.status == 200);
    REQUIRE(!res.body.empty());
    REQUIRE(res.error.empty());
}

TEST_CASE("POST works", "[networkrequest]") {
    NetworkRequest req("https://httpbin.org/post", RequestType::POST);
    req.setBody("{\"a\": \"b\"}", BodyType::JSON);
    NetworkResponse res = req.send();

    REQUIRE(res.status == 200);
    REQUIRE(!res.body.empty());
    REQUIRE(res.error.empty());
}

TEST_CASE("Status codes work", "[networkrequest]") {
    NetworkRequest req("https://httpbin.org/status/404");
    NetworkResponse res = req.send();

    REQUIRE(res.status == 404);
}
